package pl.sdacademy.lesson13;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Menu {
    static List<Human> humanList = new ArrayList<>();
    static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int input;
        do {
            System.out.print(" Wprowadź: " + "\n" + "1.Dodaj człowieka" + "\n" + "2.Usuń człowioeka" + "\n" + "3.Sprawdź czy sa równe" + "\n" + "4.Wyświetl ludzi" + "\n" + "0.Wyjście " + "\n");
            input = scanner.nextInt();
            switch (input) {
                case 1:
                    addHuman();
                    break;
                case 2:
                    deleteHuman();
                    break;
                case 3:
                    checkEqual();
                    break;
                case 4:
                    showHumans();
                    break;

            }
        } while (input != 0);
    }

    public static void addHuman() {

        Human human = new Human();
        System.out.println("Podaj imię");
        String name = scanner.next();
        System.out.println("Podaj nazwisko");
        String surnam = scanner.next();
        System.out.println("Podaj wiek");
        int age = scanner.nextInt();
        human.name = name;
        human.surname = surnam;
        human.age = age;
        humanList.add(0, human);
    }

    public static void deleteHuman() {
        System.out.println("Podaj numer osoby");
        int indexNumber = scanner.nextInt();
        humanList.remove(indexNumber);

    }

    public static boolean checkEqual() {

        System.out.println("Poda pierwszy indeks");
        int indexOne = scanner.nextInt();
        System.out.println("Poda drugi indeks");
        int indexTwo = scanner.nextInt();
        if (humanList.get(indexOne).equals(humanList.get(indexTwo))) {
            System.out.println("true");
        } else {
            System.out.println("false");
        }
        return true;

    }

    public static void showHumans() {
        for (int i = 0; i < humanList.size(); i++) {
            System.out.println(humanList.get(i));

        }

    }

}